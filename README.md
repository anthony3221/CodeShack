# CodeShack

https://code-shack.surge.sh/

## What is CodeShack?

CodeShack is a website designed to help you prepare for technical interviews. While there are many, more established sites out there that do this same thing, CodeShack is intended specifically for UW-Madison students to help narrow down their preparation. We sort all questions into categories such as courses, topics, and companies which allows even the most beginner CS student to practice with the knowledge they have. We also provide a way for users to add questions themselves, which means our content is constantly evolving and updating.

## Requirements

The only requirement is a wisc.edu google account.

Created by Anthony Leung, Sam Ware, Ted Chu, Yihu Yin, Ze Yu and Vinay Janardhanam for CS 506 at UW-Madison.